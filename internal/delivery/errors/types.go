package errors

const (
	TypeSystem        = "error/system"
	TypeBodyInvalid   = "body/invalid"
	TypePathParam     = "path/param"
	TypeDBFailed      = "db/fail"
	TypeStorageFailed = "storage/fail"
	TypeContentEmpty  = "content/empty"

	TypeUsernameNotFound = "auth/username"
	TypeUsernameDisabled = "auth/disabled"
	TypeValidation       = "auth/validation"
	TypeWrongPassword    = "auth/password"
	TypeTokenMissing     = "token/missing"
	TypeTokenInvalid     = "token/invalid"
	TypeTenantMissing    = "tenant/missing"
	TypeTenantInvalid    = "tenant/invalid"
	TypeInvalidReference = "reference/invalid"
	ErrorRequestId       = "requestId/invalid"
	NoValidData          = "request/notvalid"
	InsertToDBFailed     = "db/insert"
)
