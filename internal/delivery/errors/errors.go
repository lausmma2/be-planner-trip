package errors

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
)

func Is(err, target error) bool {
	return errors.Is(err, target)
}

func New(id, title string, code int) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  code,
		Type:  id,
		Title: title,
	}
}

func Parse(inp string) *Rfc7807Error {
	e := new(Rfc7807Error)
	err := json.Unmarshal([]byte(inp), e)
	if err != nil {
		e.Detail = inp
	}
	return e
}

// FromError try to convert go error to *Error
func FromError(err error) *Rfc7807Error {
	if verr, ok := err.(*Rfc7807Error); ok && verr != nil {
		return verr
	}

	return Parse(err.Error())
}

// BadRequest generates a 400 error.
func BadRequest(id, title string) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  400,
		Type:  id,
		Title: title,
	}
}

// Unauthorized generates a 401 error.
func Unauthorized(id, title string) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  401,
		Type:  id,
		Title: title,
	}
}

// Forbidden generates a 403 error.
func Forbidden(id, title string) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  403,
		Type:  id,
		Title: title,
	}
}

// NotFound generates a 404 error.
func NotFound(id, title string) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  404,
		Type:  id,
		Title: title,
	}
}

// MethodNotAllowed generates a 405 error.
func MethodNotAllowed(id, title string) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  405,
		Type:  id,
		Title: title,
	}
}

// Timeout generates a 408 error.
func Timeout(id, title string) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  408,
		Type:  id,
		Title: title,
	}
}

// Conflict generates a 409 error.
func Conflict(id, title string) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  409,
		Type:  id,
		Title: title,
	}
}

// InternalServerError generates a 500 error.
func InternalServerError(id, title string) *Rfc7807Error {
	return &Rfc7807Error{
		Code:  500,
		Type:  id,
		Title: title,
	}
}

// Equal tries to compare errors
func Equal(err1, err2 error) bool {
	verr1, ok1 := err1.(*Rfc7807Error)
	verr2, ok2 := err2.(*Rfc7807Error)

	if ok1 != ok2 {
		return false
	}

	if !ok1 {
		return err1 == err2
	}

	if verr1.Code != verr2.Code {
		return false
	}

	return true
}

// https://tools.ietf.org/html/rfc7807
type Rfc7807Error struct {
	Code          int                  `json:"code"`
	Type          string               `json:"type"`
	Title         string               `json:"title"`
	Detail        string               `json:"detail,omitempty"`
	Instance      string               `json:"instance,omitempty"`
	InvalidParams []invalidParamDetail `json:"invalid-params,omitempty"`
}

func (e *Rfc7807Error) Error() string {
	b, _ := json.Marshal(e)
	return string(b)
}

func (e *Rfc7807Error) PutDetail(format string, a ...interface{}) *Rfc7807Error {
	e.Detail = fmt.Sprintf(format, a...)
	return e
}

func (e *Rfc7807Error) PutParam(name, reason string, a ...interface{}) *Rfc7807Error {
	e.InvalidParams = append(e.InvalidParams, invalidParamDetail{
		Name:   name,
		Reason: fmt.Sprintf(reason, a...),
	})
	return e
}

func (e *Rfc7807Error) ToJSON(c *fiber.Ctx) error {
	return c.Status(e.Code).JSON(e)
}

type invalidParamDetail struct {
	Name   string `json:"name"`
	Reason string `json:"reason"`
}
