package delivery

import (
	"go.uber.org/fx"
	"planner-trip/internal/delivery/api"
)

var Module = fx.Options(
	api.Module,
)
