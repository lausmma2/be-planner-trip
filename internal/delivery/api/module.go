package api

import (
	"context"
	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
	"go.uber.org/fx"
	"planner-trip/internal/delivery/api/controller"
	"planner-trip/internal/delivery/api/request"
)

var Module = fx.Options(
	fx.Invoke(configLogger),
	fx.Provide(request.NewRequestParser),
	fx.Provide(NewServer),
	fx.Provide(provideFiberApp),
	fx.Invoke(register),
	controller.Module,
)

func provideFiberApp(server *Server) *fiber.App {
	return server.app
}

func configLogger() {
	log.SetLevel(log.TraceLevel)
	log.SetFormatter(&log.TextFormatter{})
}

func register(lifecycle fx.Lifecycle, server *Server) {
	lifecycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return server.Listen()
		},
		OnStop: func(ctx context.Context) error {
			return server.Shutdown()
		},
	})
}
