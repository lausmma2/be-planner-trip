package trip

import (
	"github.com/gofiber/fiber/v2"
	"planner-trip/internal/delivery/api/request"
	"planner-trip/internal/model/repository"
)

func New(rp *request.RequestParser, tripRepository *repository.TripRepository) *Controller {
	return &Controller{
		rp:             rp,
		tripRepository: tripRepository,
	}
}

type Controller struct {
	rp             *request.RequestParser
	tripRepository *repository.TripRepository
}

func (c *Controller) register(app *fiber.App) {
	app.Post("/v1/trip", c.handleV1Create)
}
