package trip

import (
	"github.com/gofiber/fiber/v2"
	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(New),
	fx.Invoke(registerController),
)

func registerController(c *Controller, app *fiber.App) {
	c.register(app)
}
