package trip

import (
	"github.com/gofiber/fiber/v2"
	"planner-trip/internal/delivery/api/request"
	"planner-trip/internal/delivery/api/response"
	"planner-trip/internal/delivery/errors"
)

func (c *Controller) handleV1Create(ctx *fiber.Ctx) error {
	req := request.V1TripRequest{}
	if rErr := c.rp.ParseAndValidateStructBody(ctx, &req); rErr != nil {
		return rErr.ToJSON(ctx)
	}

	trip, rErr := req.ToEntity()
	if rErr != nil {
		return rErr.ToJSON(ctx)
	}

	if err := c.tripRepository.Create(trip); err != nil {
		return errors.BadRequest(errors.TypeDBFailed, "Insert to db failed").
			PutDetail("Error during db query: %v", err).
			ToJSON(ctx)
	}
	tripResp, err := c.tripRepository.GetByID(trip.ID)
	if err != nil {
		return errors.BadRequest(errors.TypeDBFailed, "Insert to db failed").
			PutDetail("Error during db query: %v", err).
			ToJSON(ctx)
	}

	return ctx.JSON(response.TripToV1Response(tripResp))
}
