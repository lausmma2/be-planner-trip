package controller

import (
	"go.uber.org/fx"
	"planner-trip/internal/delivery/api/controller/place"
	"planner-trip/internal/delivery/api/controller/trip"
)

var Module = fx.Options(
	place.Module,
	trip.Module,
)
