package place

import (
	"github.com/gofiber/fiber/v2"
	"planner-trip/internal/delivery/errors"
)

func (c *Controller) handleV1DeleteByID(ctx *fiber.Ctx) error {
	id, rErr := c.rp.ParsePathUUID(ctx, "id")
	if rErr != nil {
		return rErr.ToJSON(ctx)
	}
	place, err := c.placeRepository.GetByID(id)
	if err != nil {
		return errors.NotFound(errors.TypeContentEmpty, "No content found").
			PutDetail("Place not found").
			ToJSON(ctx)
	}
	if err := c.placeRepository.DeleteByID(place.ID); err != nil {
		return errors.BadRequest(errors.TypeDBFailed, "Delete from db failed").
			PutDetail("Error during db query: %v", err).
			ToJSON(ctx)
	}
	return ctx.SendStatus(fiber.StatusOK)
}
