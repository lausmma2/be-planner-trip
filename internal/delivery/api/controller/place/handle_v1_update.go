package place

import (
	"github.com/gofiber/fiber/v2"
	"planner-trip/internal/delivery/api/request"
	"planner-trip/internal/delivery/api/response"
	"planner-trip/internal/delivery/errors"
)

func (c *Controller) handleV1Update(ctx *fiber.Ctx) error {
	id, rErr := c.rp.ParsePathUUID(ctx, "id", "is-place-id")
	if rErr != nil {
		return rErr.ToJSON(ctx)
	}
	req := request.V1PlaceRequest{}
	if rErr := c.rp.ParseAndValidateStructBody(ctx, &req); rErr != nil {
		return rErr.ToJSON(ctx)
	}

	place, rErr := req.ToEntity()
	if rErr != nil {
		return rErr.ToJSON(ctx)
	}

	err := c.placeRepository.Update(id, place)
	if err != nil {
		return errors.BadRequest(errors.TypeDBFailed, "Update to db failed").
			PutDetail("Error during db query: %v", err).
			ToJSON(ctx)
	}
	place, _ = c.placeRepository.GetByID(id)

	return ctx.JSON(response.PlaceToV1Response(place))
}
