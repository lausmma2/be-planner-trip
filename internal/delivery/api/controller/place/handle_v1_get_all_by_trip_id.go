package place

import (
	"github.com/gofiber/fiber/v2"
	"planner-trip/internal/delivery/api/response"
	"planner-trip/internal/delivery/errors"
)

func (c *Controller) handleV1GetAllByTripID(ctx *fiber.Ctx) error {
	tripId, rErr := c.rp.ParsePathUUID(ctx, "tripId")
	if rErr != nil {
		return rErr.ToJSON(ctx)
	}
	places, err := c.placeRepository.GetAllByTripID(tripId)
	if err != nil {
		return errors.NotFound(errors.TypeContentEmpty, "No content found").
			PutDetail("Places not found: %v", err).
			ToJSON(ctx)
	}

	return ctx.JSON(response.PlacesToV1Response(places))
}
