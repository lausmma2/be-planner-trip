package place

import (
	"github.com/gofiber/fiber/v2"
	"planner-trip/internal/delivery/api/request"
	"planner-trip/internal/delivery/api/response"
	"planner-trip/internal/delivery/errors"
)

func (c *Controller) handleV1Create(ctx *fiber.Ctx) error {
	req := request.V1PlaceRequest{}
	if rErr := c.rp.ParseAndValidateStructBody(ctx, &req); rErr != nil {
		return rErr.ToJSON(ctx)
	}

	place, rErr := req.ToEntity()
	if rErr != nil {
		return rErr.ToJSON(ctx)
	}

	if err := c.placeRepository.Create(place); err != nil {
		return errors.BadRequest(errors.TypeDBFailed, "Insert to db failed").
			PutDetail("Error during db query: %v", err).
			ToJSON(ctx)
	}
	placeResp, err := c.placeRepository.GetByID(place.ID)
	if err != nil {
		return errors.BadRequest(errors.TypeDBFailed, "Insert to db failed").
			PutDetail("Error during db query: %v", err).
			ToJSON(ctx)
	}

	return ctx.JSON(response.PlaceToV1Response(placeResp))
}
