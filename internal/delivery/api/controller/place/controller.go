package place

import (
	"github.com/gofiber/fiber/v2"
	"planner-trip/internal/delivery/api/request"
	"planner-trip/internal/model/repository"
)

func New(rp *request.RequestParser, placeRepository *repository.PlaceRepository) *Controller {
	return &Controller{
		rp:              rp,
		placeRepository: placeRepository,
	}
}

type Controller struct {
	rp              *request.RequestParser
	placeRepository *repository.PlaceRepository
}

func (c *Controller) register(app *fiber.App) {
	app.Post("/v1/place", c.handleV1Create)
	app.Get("/v1/place", c.handleV1GetAll)
	app.Get("/v1/place/:id", c.handleV1GetById)
	app.Get("/v1/place/trip/:tripId", c.handleV1GetAllByTripID)
	app.Delete("/v1/place/:id", c.handleV1DeleteByID)
	app.Put("/v1/place/:id", c.handleV1Update)
}
