package place

import (
	"github.com/gofiber/fiber/v2"
	"planner-trip/internal/delivery/api/response"
	"planner-trip/internal/delivery/errors"
)

func (c *Controller) handleV1GetById(ctx *fiber.Ctx) error {
	id, rErr := c.rp.ParsePathUUID(ctx, "id")
	if rErr != nil {
		return rErr.ToJSON(ctx)
	}
	place, err := c.placeRepository.GetByID(id)
	if err != nil {
		return errors.NotFound(errors.TypeContentEmpty, "No content found").
			PutDetail("Place not found").
			ToJSON(ctx)
	}

	return ctx.JSON(response.PlaceToV1Response(place))
}
