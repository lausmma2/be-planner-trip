package api

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/hashicorp/go-multierror"
	log "github.com/sirupsen/logrus"
	"planner-trip/internal/config"
	"time"
)

func NewServer(conf *config.Server) *Server {
	app := fiber.New(fiber.Config{
		IdleTimeout:           time.Second * 5,
		DisableStartupMessage: false,
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError

			// Retrieve the custom status code if it's an fiber.*Error
			if e, ok := err.(*fiber.Error); ok {
				code = e.Code
				// Send custom error page
				if err != nil {
					log.Error(err)
					// In case the SendFile fails
					return ctx.Status(code).JSON(fiber.Map{
						"message": err.(*fiber.Error).Message,
						"code":    code,
						"time":    time.Now(),
					})
				}
			} else {
				// Send custom error page
				if err != nil {
					// In case the SendFile fails
					log.Error(err)
					return ctx.Status(code).JSON(fiber.Map{
						"message": err,
						"code":    400,
						"time":    time.Now(),
					})
				}
			}

			// Return from handler
			return nil
		},
	})

	app.Use(recover.New())
	app.Use(compress.New())
	app.Use(recover.New(recover.Config{EnableStackTrace: true}))
	/*if appConf.Env == "production" {
		// To initialize Sentry's handler, you need to initialize Sentry itself beforehand
		if err := sentry.Init(sentry.ClientOptions{
			Dsn:              "https://568c545f7587423abe875b5ba3d90c44@o951161.ingest.sentry.io/5910059",
			AttachStacktrace: true,
			Release:          "unreleased",
			Environment:      "dev",
		}); err != nil {
			log.Errorf("Sentry initialization failed: %v\n", err)
		}
		// Create an instance of sentryfiber
		sentryHandler := sentryfiber.New(sentryfiber.Options{Repanic: true})
		app.Use(sentryHandler)
	} else {
		app.Use(logger.New(logger.ConfigDefault))
	}*/

	app.Use(logger.New(logger.ConfigDefault))

	app.Use(cors.New(cors.Config{
		// TODO better CORS from config
		AllowOrigins: "http://localhost, http://localhost:3000, http://localhost:4000",
	}))

	server := &Server{
		app:  app,
		conf: conf,
	}

	return server
}

type Server struct {
	app  *fiber.App
	conf *config.Server
}

func (s *Server) Listen() error {
	log.Info("Server listening on %s\n", s.conf.GetListenAddr())
	go s.app.Listen(s.conf.GetListenAddr())
	return nil
}

func (s *Server) Shutdown() error {
	var result error
	if err := s.app.Shutdown(); err != nil {
		result = multierror.Append(result, err)
	}

	return result
}
