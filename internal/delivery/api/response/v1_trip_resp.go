package response

import (
	"github.com/google/uuid"
	"planner-trip/internal/model/entity"
)

type V1TripResponse struct {
	ID          uuid.UUID `json:"id" example:"c36c037d-d23e-469d-bffe-4d03fa81687f" validate:""`
	Title       string    `json:"title" example:"Trip to Kunka"`
	Description string    `json:"description" example:"Good trip"`
	Creator     string    `json:"creator" example:"Pepa"`
	Identifier  string    `json:"identifier" example:"AACC"`
	//StartAt   entity.DateTime `json:"startAt" example:"2021-09-07T15:31:49.6293359+02:00"`
	//EndAt     entity.DateTime `json:"endAt" example:"2021-09-07T15:31:49.6293359+02:00"`
	CreatedAt entity.DateTime `json:"createdAt" example:"2021-09-07T15:31:49.6293359+02:00"`
	UpdatedAt entity.DateTime `json:"updatedAt" example:"2021-09-07T15:31:49.6293359+02:00"`
}

func TripToV1Response(p *entity.Trip) V1TripResponse {
	resp := V1TripResponse{
		ID:          p.ID,
		Title:       p.Title,
		Description: p.Description,
		Creator:     p.Creator,
		Identifier:  p.Identifier,
		CreatedAt:   entity.DateTime(p.CreatedAt),
		UpdatedAt:   entity.DateTime(p.UpdatedAt),
	}
	/*if b.Image != nil {
		image := FileToV1Response(b.Image)
		resp.Image = &image
	}*/
	return resp
}

func TripsToV1Response(b []*entity.Trip) []V1TripResponse {
	resp := make([]V1TripResponse, len(b))
	for i, s := range b {
		resp[i] = TripToV1Response(s)
	}
	return resp
}
