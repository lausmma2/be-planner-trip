package response

import (
	"github.com/google/uuid"
	"planner-trip/internal/model/entity"
)

type V1PlaceResponse struct {
	ID        uuid.UUID       `json:"id" example:"c36c037d-d23e-469d-bffe-4d03fa81687f" validate:""`
	Title     string          `json:"title" example:"Kunětická hora"`
	Latitude  string          `json:"latitude" example:"38.8951"`
	Longitude string          `json:"longitude" example:"-77.0364"`
	Distance  int64           `json:"distance" example:"2500"`
	TripID    uuid.UUID       `json:"tripId" example:"c36c037d-d23e-469d-bffe-4d03fa81687f"`
	CreatedAt entity.DateTime `json:"createdAt" example:"2021-09-07T15:31:49.6293359+02:00"`
	UpdatedAt entity.DateTime `json:"updatedAt" example:"2021-09-07T15:31:49.6293359+02:00"`
}

func PlaceToV1Response(p *entity.Place) V1PlaceResponse {
	resp := V1PlaceResponse{
		ID:        p.ID,
		Title:     p.Title,
		Latitude:  p.Latitude,
		Longitude: p.Longitude,
		Distance:  p.Distance,
		TripID:    p.TripID,
		CreatedAt: entity.DateTime(p.CreatedAt),
		UpdatedAt: entity.DateTime(p.UpdatedAt),
	}
	/*if b.Image != nil {
		image := FileToV1Response(b.Image)
		resp.Image = &image
	}*/
	return resp
}

func PlacesToV1Response(b []*entity.Place) []V1PlaceResponse {
	resp := make([]V1PlaceResponse, len(b))
	for i, s := range b {
		resp[i] = PlaceToV1Response(s)
	}
	return resp
}
