package request

import (
	"github.com/google/uuid"
	"planner-trip/internal/delivery/errors"
	"planner-trip/internal/model/entity"
)

type V1PlaceRequest struct {
	Title     string  `json:"title" validate:"required" example:"Kunětická hora"`
	Latitude  string  `json:"latitude" validate:"required" example:"38.8951"`
	Longitude string  `json:"longitude" validate:"required" example:"-77.0364"`
	Distance  int64   `json:"distance" validate:"required" example:"500"`
	TripID    *string `json:"tripId" validate:"required" example:"c36c037d-d23e-469d-bffe-4d03fa81687f"`
}

func (r V1PlaceRequest) ToEntity() (*entity.Place, *errors.Rfc7807Error) {

	tripID, err := uuid.Parse(*r.TripID)
	if err != nil {
		return nil, errors.BadRequest(errors.ErrorRequestId, "Invalid request").
			PutDetail("Invalid uuid").
			PutParam("tripId", "invalid uuid: %v", err)
	}

	place := &entity.Place{
		Title:     r.Title,
		Latitude:  r.Latitude,
		Longitude: r.Longitude,
		Distance:  r.Distance,
		TripID:    tripID,
	}

	return place, nil
}
