package request

import (
	"planner-trip/internal/delivery/errors"
	"planner-trip/internal/model/entity"
)

type V1TripRequest struct {
	Title       string    `json:"title" validate:"required" example:"Trip to Kunka"`
	Description string    `json:"description" validate:"required" example:"This is good trip"`
	Creator     string    `json:"creator" validate:"required" example:"User Pepa"`
	Identifier  string    `json:"identifier" validate:"required" example:"AABB"`
	Places      []*string `json:"places" validate:"required" example:"c36c037d-d23e-469d-bffe-4d03fa81687f"`
	StartAt     *string   `json:"startAt" validate:"omitempty" example:"2021-09-07T15:31:49.6293359+02:00"`
	EndAt       *string   `json:"endAt" validate:"omitempty" example:"2021-09-07T15:31:49.6293359+02:00"`
}

func (r V1TripRequest) ToEntity() (*entity.Trip, *errors.Rfc7807Error) {

	trip := &entity.Trip{
		Title:       r.Title,
		Description: r.Description,
		Creator:     r.Creator,
		Identifier:  r.Identifier,
	}

	return trip, nil
}
