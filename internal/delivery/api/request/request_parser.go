package request

import (
	"mime/multipart"
	"planner-trip/internal/delivery/errors"
	"planner-trip/internal/model/entity/enum"
	"strconv"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
)

func NewRequestParser(validate *validator.Validate) *RequestParser {
	return &RequestParser{
		validate: validate,
	}
}

type RequestParser struct {
	validate *validator.Validate
}

func (*RequestParser) ParseQueryInt(ctx *fiber.Ctx, name string, def int) int {
	param := ctx.Query(name)
	i, err := strconv.Atoi(param)
	if err != nil || i < 0 {
		return def
	}
	return i
}

func (*RequestParser) ParseQueryString(ctx *fiber.Ctx, name string) string {
	param := ctx.Query(name)
	return param
}

func (rp *RequestParser) ParseAndValidateQueryString(ctx *fiber.Ctx, name string, validateTags ...string) (string, *errors.Rfc7807Error) {
	param := ctx.Query(name)

	values := []string{param}
	if strings.Contains(param, ",") {
		values = strings.Split(param, ",")
	}

	for _, value := range values {
		for _, tag := range validateTags {
			err := rp.validate.VarCtx(ctx.Context(), value, tag)
			if err != nil {
				resultErr := errors.BadRequest(errors.TypeBodyInvalid, "Invalid request parameter").
					PutDetail("Parameter validation problem")
				for _, fe := range err.(validator.ValidationErrors) {
					resultErr = resultErr.PutParam(name, "invalid for rule %s", fe.ActualTag()).
						PutDetail("invalid value %s", value)
				}

				return param, resultErr
			}
		}
	}

	return param, nil
}

func (rp *RequestParser) ParsePathUUID(ctx *fiber.Ctx, name string, validateTags ...string) (uuid.UUID, *errors.Rfc7807Error) {
	param := ctx.Params(name)
	validateTags = append(validateTags, "uuid")
	for _, tag := range validateTags {
		err := rp.validate.VarCtx(ctx.Context(), param, tag)
		if err != nil {
			resultErr := errors.BadRequest(errors.TypeBodyInvalid, "Invalid request parameter").
				PutDetail("Parameter validation problem")
			for _, fe := range err.(validator.ValidationErrors) {
				resultErr = resultErr.PutParam(name, "invalid for rule %s", fe.ActualTag())
			}

			return uuid.UUID{}, resultErr
		}
	}

	id, err := uuid.Parse(param)
	if err != nil {
		return id, errors.BadRequest(errors.TypePathParam, "Invalid request").
			PutDetail("Invalid path parameter").
			PutParam(name, "invalid uuid: %v", err)
	}

	return id, nil
}

func (*RequestParser) ParsePathFileType(ctx *fiber.Ctx, name string) (enum.FileType, *errors.Rfc7807Error) {
	val := ctx.Params(name)
	if val != "image" && val != "file" {
		return "", errors.BadRequest(errors.TypePathParam, "Invalid request").
			PutDetail("Invalid path parameter").
			PutParam(name, "invalid file type: %s", val)
	}

	return enum.FileType(val), nil
}

func (*RequestParser) ParsePathString(ctx *fiber.Ctx, name string) string {
	return ctx.Params(name)
}

func (rp *RequestParser) ParseBodyUUIDs(ctx *fiber.Ctx, validateTags ...string) ([]uuid.UUID, *errors.Rfc7807Error) {
	var out []string
	if err := ctx.BodyParser(&out); err != nil {
		return nil, errors.BadRequest(errors.TypeBodyInvalid, "Invalid request body").
			PutDetail("Cannot parse request body: %v", err)
	}
	for _, tag := range validateTags {
		for _, id := range out {
			if err := rp.validate.VarCtx(ctx.Context(), id, tag); err != nil {
				resultErr := errors.BadRequest(errors.TypeBodyInvalid, "Invalid request body").
					PutDetail("Request body validation problem")
				if validErrs, ok := err.(validator.ValidationErrors); ok {
					for _, fe := range validErrs {
						resultErr = resultErr.PutParam(fe.Field(), "invalid for rule %s", fe.ActualTag())
					}
				}
				return nil, resultErr
			}
		}
	}
	uuids := make([]uuid.UUID, len(out))
	for i, str := range out {
		id, err := uuid.Parse(str)
		if err != nil {
			return nil, errors.BadRequest(errors.TypePathParam, "Invalid request").
				PutDetail("Invalid body parameter: %v", err)
		}
		uuids[i] = id
	}
	return uuids, nil
}

func (rp *RequestParser) ParseAndValidateStructBody(ctx *fiber.Ctx, out interface{}) *errors.Rfc7807Error {
	if err := rp.ParseBody(ctx, out); err != nil {
		return err
	}

	if err := rp.ValidateStructBody(ctx, out); err != nil {
		return err
	}

	return nil
}

func (rp *RequestParser) ValidateStructBody(ctx *fiber.Ctx, out interface{}) *errors.Rfc7807Error {
	if err := rp.validate.StructCtx(ctx.Context(), out); err != nil {
		log.Error(err.Error())
		resultErr := errors.BadRequest(errors.TypeBodyInvalid, "Invalid request body").
			PutDetail("Request body validation problem")
		if validErrs, ok := err.(validator.ValidationErrors); ok {
			for _, fe := range validErrs {
				resultErr = resultErr.PutParam(fe.Field(), "invalid for rule %s", fe.ActualTag())
			}
		}
		return resultErr
	}

	return nil
}

func (*RequestParser) ParseBody(ctx *fiber.Ctx, out interface{}) *errors.Rfc7807Error {
	if err := ctx.BodyParser(out); err != nil {
		return errors.BadRequest(errors.TypeBodyInvalid, "Invalid request body").
			PutDetail("Cannot parse request body: %v", err)
	}

	return nil
}

func (*RequestParser) ParseMultipartForm(ctx *fiber.Ctx, name string) ([]*multipart.FileHeader, *errors.Rfc7807Error) {
	form, err := ctx.MultipartForm()
	if err != nil {
		return nil, errors.BadRequest(errors.TypeBodyInvalid, "Invalid request body").
			PutDetail("Error using multipart form: %v", err)
	}

	files, ok := form.File[name]
	if !ok || len(files) == 0 {
		return nil, errors.BadRequest(errors.TypeBodyInvalid, "Invalid request body").
			PutDetail("Error getting multipart field").
			PutParam(name, "field is empty")
	}

	return files, nil
}
