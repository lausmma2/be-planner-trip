package database

import (
	"gorm.io/gorm/logger"
	"planner-trip/internal/config"
	"time"

	log "github.com/sirupsen/logrus"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	_ "github.com/lib/pq"
)

func New(conf *config.Database) (*gorm.DB, error) {
	dbLogger := logger.New(
		log.StandardLogger(),
		logger.Config{
			SlowThreshold:             50000000, // 500ms
			Colorful:                  false,
			LogLevel:                  logger.Info,
			IgnoreRecordNotFoundError: true,
		},
	)

	db, err := gorm.Open(postgres.Open(conf.GetDatabaseDsn()), &gorm.Config{
		Logger: dbLogger,
	})
	if err != nil {
		return nil, err
	}

	sqlDB, err := db.DB()
	if err != nil {
		return nil, err
	}

	sqlDB.SetMaxIdleConns(10)
	sqlDB.SetMaxOpenConns(100)
	sqlDB.SetConnMaxLifetime(time.Hour)

	return db, nil
}
