package entity

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type Place struct {
	ID        uuid.UUID `gorm:"type:uuid;primaryKey"`
	TripID    uuid.UUID
	Title     string
	Latitude  string
	Longitude string
	Distance  int64
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (Place) TableName() string {
	return "place"
}

func (p *Place) BeforeCreate(_ *gorm.DB) error {
	p.ID = uuid.New()
	return nil
}
