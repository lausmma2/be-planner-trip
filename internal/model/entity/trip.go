package entity

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type Trip struct {
	ID          uuid.UUID `gorm:"type:uuid;primaryKey"`
	Title       string
	Description string
	Identifier  string
	Creator     string
	StartAt     time.Time
	EndAt       time.Time
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func (Trip) TableName() string {
	return "trip"
}

func (t *Trip) BeforeCreate(_ *gorm.DB) error {
	t.ID = uuid.New()
	return nil
}
