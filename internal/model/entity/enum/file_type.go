package enum

import (
	"database/sql/driver"
	"strings"
)

type FileType string

const (
	FileTypeImage FileType = "image"
	FileTypeFile  FileType = "file"
	FileTypeFont  FileType = "font"
)

func (e *FileType) Scan(value interface{}) error {
	strValue := value.(string)
	*e = FileType(strValue)
	return nil
}

func (e FileType) Value() (driver.Value, error) {
	return string(e), nil
}

func FileTypeFromMimeType(mimeType string) FileType {
	if strings.HasPrefix(mimeType, "image/") {
		return FileTypeImage
	}
	if strings.HasPrefix(mimeType, "font/") {
		return FileTypeFont
	}
	return FileTypeFile
}
