package entity

import (
	"database/sql/driver"
	"fmt"
	"planner-trip/internal/delivery/errors"
	"strings"
	"time"
)

const DateLayout = "2006-01-02"
const DatetimeLayout = time.RFC3339

type Marshaller interface {
	MarshalJSON() ([]byte, error)
	UnmarshalJSON(data []byte) error
}

type Date time.Time

func (d Date) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", time.Time(d).Format(DateLayout))
	return []byte(stamp), nil
}

func (d *Date) UnmarshalJSON(data []byte) error {
	s := strings.Trim(string(data), "\"")
	if s == "null" {
		return nil
	}

	var err error
	parsed, err := time.Parse(DateLayout, s)
	*d = Date(parsed)

	return err
}

// Scan scan value into Jsonb, implements sql.Scanner interface
func (d *Date) Scan(value any) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(errors.TypeBodyInvalid, fmt.Sprint("Failed to unmarshal JSONB value:", value), 500)
	}

	parsed, err := time.Parse(DateLayout, string(bytes))
	*d = Date(parsed)
	return err
}

func (d Date) Value() (driver.Value, error) {
	return time.Time(d).Format(DateLayout), nil
}

type DateTime time.Time

func (dt DateTime) MarshalJSON() ([]byte, error) {
	stamp := dt.DefaultFormat()
	return []byte(stamp), nil
}

func (dt DateTime) DefaultFormat() string {
	stamp := fmt.Sprintf("\"%s\"", time.Time(dt).Format(DatetimeLayout))
	return stamp
}

func (dt *DateTime) UnmarshalJSON(data []byte) error {
	s := strings.Trim(string(data), "\"")
	if string(s) == "null" {
		return nil
	}

	var err error
	parsed, err := time.Parse(DatetimeLayout, s)
	*dt = DateTime(parsed)

	return err
}

// Scan scan value into Jsonb, implements sql.Scanner interface
func (dt *DateTime) Scan(value any) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(errors.TypeBodyInvalid, fmt.Sprint("Failed to unmarshal JSONB value:", value), 500)
	}

	parsed, err := time.Parse(DatetimeLayout, string(bytes))
	*dt = DateTime(parsed)
	return err
}

func (dt DateTime) Value() (driver.Value, error) {
	return time.Time(dt).Format(DatetimeLayout), nil
}
