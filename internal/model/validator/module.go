package validator

import (
	"github.com/go-playground/validator/v10"
	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(validator.New),
	fx.Invoke(registerIsPlaceIdValidator),
)
