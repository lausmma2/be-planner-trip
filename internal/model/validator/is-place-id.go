package validator

import (
	"context"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"planner-trip/internal/model/repository"
)

func registerIsPlaceIdValidator(v *validator.Validate, repo *repository.PlaceRepository) error {
	return v.RegisterValidationCtx("is-place-id", func(ctx context.Context, fl validator.FieldLevel) bool {
		id, err := uuid.Parse(fl.Field().String())
		if err != nil {
			log.Debug(err)
			return false
		}
		place, err := repo.GetByID(id)
		if err != nil {
			return false
		}
		return place != nil
	})
}
