package model

import (
	"go.uber.org/fx"
	"planner-trip/internal/model/repository"
	"planner-trip/internal/model/validator"
)

var Module = fx.Options(
	validator.Module,
	repository.Module,
)
