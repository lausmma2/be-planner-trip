package repository

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"planner-trip/internal/model/entity"
)

func NewPlaceRepository(db *gorm.DB) *PlaceRepository {
	return &PlaceRepository{
		db: db,
	}
}

type PlaceRepository struct {
	db *gorm.DB
}

func (r *PlaceRepository) Create(place *entity.Place) error {
	return r.db.Create(&place).Error
}

func (r *PlaceRepository) GetByID(id uuid.UUID) (*entity.Place, error) {
	var place entity.Place
	// Nešlo by to tady přes Model?
	if err := r.db.
		Where("id = ?", id).
		First(&place).Error; err != nil {
		return nil, err
	}
	return &place, nil
}

func (r *PlaceRepository) DeleteByID(id uuid.UUID) error {
	return r.db.Delete(&entity.Place{}, id).Error
}

func (r *PlaceRepository) GetAll() ([]*entity.Place, error) {
	var places []*entity.Place
	err := r.db.
		//Preload("Products").
		Order("created_at asc").
		Find(&places).Error

	if err != nil {
		return nil, err
	}

	return places, nil
}

func (r *PlaceRepository) GetAllByTripID(tripId uuid.UUID) ([]*entity.Place, error) {
	var places []*entity.Place
	err := r.db.
		Where("trip_id = ?", tripId).
		Order("created_at asc").
		Find(&places).Error

	if err != nil {
		return nil, err
	}

	return places, nil
}

func (r *PlaceRepository) Update(id uuid.UUID, place *entity.Place) error {
	return r.db.Model(&entity.Place{}).Where("id = ?", id).Updates(place).Error
}
