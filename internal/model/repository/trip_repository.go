package repository

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"planner-trip/internal/model/entity"
)

func NewTripRepository(db *gorm.DB) *TripRepository {
	return &TripRepository{
		db: db,
	}
}

type TripRepository struct {
	db *gorm.DB
}

func (r *TripRepository) Create(trip *entity.Trip) error {
	return r.db.Create(&trip).Error
}

func (r *TripRepository) GetByID(id uuid.UUID) (*entity.Trip, error) {
	var trip entity.Trip
	// Nešlo by to i tady přes Model?
	if err := r.db.
		Where("id = ?", id).
		First(&trip).Error; err != nil {
		return nil, err
	}
	return &trip, nil
}

func (r *TripRepository) GetAll() ([]*entity.Trip, error) {
	var trips []*entity.Trip
	err := r.db.
		//Preload("Products").
		Order("created_at asc").
		Find(&trips).Error

	if err != nil {
		return nil, err
	}

	return trips, nil
}

func (r *TripRepository) Update(id uuid.UUID, trip *entity.Trip) error {
	return r.db.Model(&entity.Trip{}).Where("id = ?", id).Updates(trip).Error
}
