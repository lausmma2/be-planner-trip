package internal

import (
	log "github.com/sirupsen/logrus"
	"go.uber.org/fx"
	"planner-trip/internal/config"
	"planner-trip/internal/database"
	"planner-trip/internal/delivery"
	"planner-trip/internal/model"
)

func CreateApp(additional ...fx.Option) *fx.App {
	fx.WithLogger(log.StandardLogger())

	return fx.New(
		config.Module,
		database.Module,
		delivery.Module,
		model.Module,
		fx.Options(additional...),
	)
}
