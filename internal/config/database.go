package config

import "fmt"

type Database struct {
	Driver   string `default:"postgres"`
	Host     string
	Port     string
	User     string
	Password string
	Database string
	Args     string `default:"parseTime=true"`
}

func (c *Database) GetDatabaseDsn() string {
	if c.Driver == "postgres" {
		return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s %s", c.Host, c.Port, c.User, c.Password, c.Database, c.Args)
	}

	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s", c.User, c.Password, c.Host, c.Port, c.Database, c.Args)
}
