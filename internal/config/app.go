package config

import (
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type App struct {
	Env      string    `default:"development"`
	Name     string    `default:"backend-core"`
	Database *Database `envconfig:"DB"`
	Server   *Server   `envconfig:"SERVER"`
}

func New() (*App, error) {
	_ = godotenv.Load()

	var conf App
	if err := envconfig.Process("APP", &conf); err != nil {
		return nil, err
	}

	return &conf, nil
}
