package config

import "go.uber.org/fx"

var Module = fx.Options(
	fx.Provide(New),
	fx.Provide(provideDatabaseConfig),
	fx.Provide(provideServerConfig),
)

func provideDatabaseConfig(app *App) *Database {
	return app.Database
}

func provideServerConfig(app *App) *Server {
	return app.Server
}
