CREATE TABLE "public"."trip"
(
    "id"          uuid              NOT NULL,
    "title"       character varying NOT NULL,
    "description" character varying,
    "creator"     character varying NOT NULL,
    "identifier"  character varying NOT NULL,
    "start_at"    timestamp,
    "end_at"      timestamp,
    "created_at"  timestamp default now(),
    "updated_at"  timestamp default now(),
    CONSTRAINT "trip_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE TABLE "public"."place"
(
    "id"         uuid              NOT NULL,
    "title"      character varying NOT NULL,
    "latitude"   character varying NOT NULL,
    "longitude"  character varying NOT NULL,
    "distance"   int               NOT NULL,
    "trip_id"    uuid              NOT NULL,
    "created_at" timestamp default now(),
    "updated_at" timestamp default now(),
    CONSTRAINT "place_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

ALTER TABLE "public"."place"
    ADD FOREIGN KEY ("trip_id") REFERENCES "trip" ("id") ON DELETE CASCADE;
