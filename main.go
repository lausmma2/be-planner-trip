package main

import (
	"planner-trip/internal"
)

func main() {
	internal.CreateApp().Run()
}
