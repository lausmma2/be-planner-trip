# Include environment variables if possible
-include .env
#-include .env.production
TIME := $(shell date +"%Y%m%d%H%M%S")

all: proto-install proto install test build

swag-install:
	go install github.com/swaggo/swag/cmd/swag

install: proto-install swag-install swag
	go install
	flyway > /dev/null && echo "OK" || echo "Install FLYWAY from https://flywaydb.org/download/community"

proto-install:
	go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc

proto:
	echo $$PATH
	rm **/**/*.pb.go ***/*.pb.go *.pb.go || echo "OK"
	protoc -I=submodule/proto/ --experimental_allow_proto3_optional \
		--go_out=./proto \
		--go-grpc_out=./proto \
		submodule/proto/authenticator.proto submodule/proto/authorizator.proto

swag:
	echo $$PATH
	swag init --parseDependency --parseDepth 4 --parseInternal

test: migrate-reset
	go test ./... -v

build: swag proto
	go build -v -ldflags="-w -s" -o output/app

migrate:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations -outOfOrder=true migrate

migrate-down:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations undo

migrate-drop:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations clean

migrate-status:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations info

migrate-reset: migrate-drop migrate

migrate-create:
	touch migrations/V${TIME}__RENAME.sql
	touch migrations/U${TIME}__RENAME.sql

pretty:
	go fmt $(go list ./... | grep -v /vendor/)

.PHONY: test proto

start: swag
	go run main.go

update:
	go get -u
	go mod tidy
	go get -u github.com/swaggo/swag/gen
	go get -u github.com/swaggo/swag/cmd/swag
	go get -u gorm.io/driver/postgres
	go get -u github.com/golang-migrate/migrate/v4/cmd/migrate
